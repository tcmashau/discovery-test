package za.co.discovery.msrv.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import za.co.discovery.msrv.vo.User;
import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Component
public class Users {

    private ObjectMapper objectMapper = new ObjectMapper();
    private String home = System.getProperty("user.home") + File.separator + "discovery-test" + File.separator;

    public User save(User user) throws Exception {

        String id = RandomStringUtils.randomAlphanumeric(10);
        String fileName = home + id;
        File folder = new File(home);
        if(!folder.exists()) {
            folder.mkdir();
        }
        File myFile = new File(fileName);
        OutputStream os = null;
        try {
            if (myFile.createNewFile()) {
                os = new FileOutputStream(fileName);
                user.setId(id);
                objectMapper.writeValue(os, user);
            }
        } catch (Throwable e) {
            throw new Exception(e);
        } finally {
            IOUtils.closeQuietly(os);
        }
        return user;

    }

    public User get(String id) {

        try {
            InputStream is = new FileInputStream(home + id);
            return objectMapper.readValue(is, User.class);
        } catch (Throwable e) {
        }
        return new User();

    }

    public List<User> getUsers() {

        File currentDir = new File(home);
        final List<User> users = new LinkedList<User>();
        currentDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                User user = null;
                try {
                    user = objectMapper.readValue(pathname, User.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                users.add(user);
                return true;
            }
        });
        return users;

    }

}
