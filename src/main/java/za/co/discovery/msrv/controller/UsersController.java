package za.co.discovery.msrv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.discovery.msrv.dao.Users;
import za.co.discovery.msrv.vo.User;
import java.util.List;

@RequestMapping("/users")
@RestController
public class UsersController {

    @Autowired
    private Users users;

    @RequestMapping(method = RequestMethod.PUT)
    public User save(@RequestBody User user) throws  Exception {
        return users.save(user);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/allusers")
    public List<User> search() throws  Exception {
        return users.getUsers();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/{id}")
    public User getUser(@PathVariable("id") String id) throws  Exception {
        return users.get(id);
    }


}
