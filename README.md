## Discovery Angular App Test

This is a short angular test to evaluate the candidate's basic knowledge of Angular 1.
The application uses http port 8080. Before running the application, make sure nothing else is utilizing http port 8080.

## Skills required to complete the test

1.  Maven
2.  Basic understanding of REST WEBSERVICES
3.  Angular 1
4.  git clone
5.  git commit
6.  git push.

## Code Structure

The only files that the candidate should work on are under the folder src/main/resources/static
The candidate is allowed to add new files under this folder if need be.

## Installation

1.  Clone the project into your local machine.
2.  Run the main function in src/main/java/za/co/discovery/msrv/Application, this should start an embeded tomcat server on http port 8080.
3.  Navigate to http://localhost:8080 you should see the text below

    ```
    Hello world
    ```

## Available rest webservices operations

* **Add User** - *put* - http://localhost:8080/users

* Required data

    ```
    {
      "date": "2016-06-08T17:14:47.335Z",
      "firstName": "test",
      "lastName": "user"
    }
    ```

* **Retrieve users** - *get* - http://localhost:8080/users/allusers

* Response data

    ```
    [
         {
            id: "KSkXXWDk10",
            date: 1465400700199,
            firstName: "Test1",
            lastName: "User1"
         },
         {
            id: "tLs9zi948m",
            date: 1465400700199,
            firstName: "Test2",
            lastName: "User2"
         },
         {
            id: "wArtmgw2KZ",
            date: 1465406087335,
            firstName: "test3",
            lastName: "user3"
         }
    ]
    ```

* **Retrieve user** - *get* - http://localhost:8080/users/{id}

* Response data

    ```
    {
        id: "KSkXXWDk10",
        date: 1465400700199,
        firstName: "Test1",
        lastName: "User1"
    }
    ```

## **Task to be completed**

1. Use the existing form to add a new user using angular submit and http operations
2. Create a date picker directive that will be used on the date field.

## Submitting the test

Commit your code, push it to the remote repository and notify the agent that gave you the assignment.